package ru.nsu.ggk.sl.view;

import ru.nsu.ggk.sl.controller.ChiefWindowController;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.awt.image.BufferedImage;
import java.io.File;

public class VisitShopWindowView extends JFrame {

    private JFrame visitShopFrame;
    private JLabel studentPicture;

    private JLabel getStudentPicture() {
        return studentPicture;
    }

    private JFrame getVisitShopFrame() {
        return visitShopFrame;
    }

    public void createVisitShopWindow(ChiefWindowController chiefWindowController) {
        visitShopFrame = new JFrame("Поход в магазин");
        visitShopFrame.getContentPane().setBackground(Color.white);

        BufferedImage bufferedPicture = null;
        try {
            switch (chiefWindowController.getPersonageModelController().getPersonageModel().getStudentType())
            {
                case "Бакалавр":
                    bufferedPicture = ImageIO.read(new File("resourses/bacalavr_wait.png"));
                    break;
                case "Магистр":
                    bufferedPicture = ImageIO.read(new File("resourses/magistrant_wait.png"));
                    break;
                case "Аспирант":
                    bufferedPicture = ImageIO.read(new File("resourses/aspirant_wait.png"));
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        studentPicture = new JLabel(new ImageIcon(bufferedPicture));

        JButton buyFoodButton = new JButton("Купить продукты в тц");

        buyFoodButton.addActionListener(e -> {
            switch (chiefWindowController.getPersonageModelController().getPersonageModel().getStudentType()) {
                case "Бакалавр":
                    try {
                        BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/bacalavr_food.png"));
                        chiefWindowController.getVisitShopWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
                case "Магистр":
                    try {
                        BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/magistrant_food.png"));
                        chiefWindowController.getVisitShopWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
                case "Аспирант":
                    try {
                        BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/aspirant_food.png"));
                        chiefWindowController.getVisitShopWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
            }

            int coinsAmount = chiefWindowController.getPersonageModelController().getPersonageModel().getCoinsAmount();
            if (coinsAmount - 10 < 0) {
                return;
            }
            coinsAmount -= 10;
            chiefWindowController.getPersonageModelController().getPersonageModel().setCoinsAmount(coinsAmount);

            int foodAmount = chiefWindowController.getPersonageModelController().getPersonageModel().getFoodAmount();
            foodAmount++;
            chiefWindowController.getPersonageModelController().getPersonageModel().setFoodAmount(foodAmount);
            chiefWindowController.getPersonageModelController().getModelPropertyChange().firePropertyChange("OwnerProperty", 0, 1);
        });

        JButton buyDrinkButton = new JButton("Купить еду в столовой");

        buyDrinkButton.addActionListener(e ->
        {    switch (chiefWindowController.getPersonageModelController().getPersonageModel().getStudentType()) {
            case "Бакалавр":
                try {
                    BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/bacalavr_food.png"));
                    chiefWindowController.getVisitShopWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
                break;
            case "Магистр":
                try {
                    BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/magistrant_food.png"));
                    chiefWindowController.getVisitShopWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
                break;
            case "Аспирант":
                try {
                    BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/aspirant_food.png"));
                    chiefWindowController.getVisitShopWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
                break;
        }
            int coinsAmount = chiefWindowController.getPersonageModelController().getPersonageModel().getCoinsAmount();
            if (coinsAmount - 5 < 0) {
                return;
            }
            coinsAmount -= 5;
            chiefWindowController.getPersonageModelController().getPersonageModel().setCoinsAmount(coinsAmount);

            int drinkAmount = chiefWindowController.getPersonageModelController().getPersonageModel().getDrinkAmount();
            drinkAmount++;
            chiefWindowController.getPersonageModelController().getPersonageModel().setDrinkAmount(drinkAmount);
            chiefWindowController.getPersonageModelController().getModelPropertyChange().firePropertyChange("OwnerProperty", 0, 1);
        });

        JButton returnHomeVisitShopButton = new JButton("Вернуться домой");
        returnHomeVisitShopButton.addActionListener(e -> {
            chiefWindowController.getVisitShopWindow().getVisitShopFrame().setVisible(false);
        });

        buyFoodButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        buyDrinkButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        returnHomeVisitShopButton.setAlignmentX(Component.CENTER_ALIGNMENT);

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setBackground(Color.white);
        buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.Y_AXIS));

        buttonsPanel.add(buyFoodButton);
        buttonsPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        buttonsPanel.add(buyDrinkButton);
        buttonsPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        buttonsPanel.add(returnHomeVisitShopButton);

        visitShopFrame.setLayout(new FlowLayout());
        visitShopFrame.setSize(430, 260);

        visitShopFrame.add(studentPicture);
        visitShopFrame.add(buttonsPanel);
        visitShopFrame.setIconImage(new ImageIcon("resourses/icon.png").getImage());
        visitShopFrame.setLocationRelativeTo(null);
        visitShopFrame.setVisible(true);
    }
}


