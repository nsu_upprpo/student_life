package ru.nsu.ggk.sl.view;

import ru.nsu.ggk.sl.controller.ChiefWindowController;
import ru.nsu.ggk.sl.model.Student;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class CreateStudentWindowView extends JFrame {
    private JFrame createStudentFrame;
    private JTextField personageNameInput;
    private JComboBox comboStudents;
    private JTextField personageAgeInput;
    private JLabel personageLabel;

    private JTextField getStudentAgeInput() {
        return personageAgeInput;
    }

    private JTextField getStudentNameInput() {
        return personageNameInput;
    }

    private JLabel getStudentLabel() {
        return personageLabel;
    }

    private JFrame getCreateStudentFrame() {
        return createStudentFrame;
    }

    private String getStudentName() {
        return personageNameInput.getText();
    }

    public String getStudentType() {
        return (String) comboStudents.getSelectedItem();
    }

    private int getStudentAge() {
        try {
            return Integer.parseInt(personageAgeInput.getText());
        } catch (Exception e) {
            return 1;
        }
    }

    public void createSelectStudentWindow(ChiefWindowController chiefWindowController) {
        createStudentFrame = new JFrame("Выбор студента");
        createStudentFrame.getContentPane().setBackground(Color.white);

        BufferedImage bufferedPicture = null;
        try {
            bufferedPicture = ImageIO.read(new File("resourses/bacalavr_main.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        personageLabel = new JLabel(new ImageIcon(bufferedPicture));

        JLabel personageName = new JLabel("Имя студента:");
        JLabel personageAge = new JLabel("Возраст студента:");
        JLabel personageType = new JLabel("Выберите студента:");

        personageNameInput = new JTextField("Бакалавр");
        personageNameInput.setHorizontalAlignment(SwingConstants.CENTER);
        personageNameInput.setCaretColor(Color.white);
        personageNameInput.setPreferredSize(new Dimension(200, 30));
        personageNameInput.setEditable(false);

        personageAgeInput = new JTextField("20");
        personageAgeInput.setHorizontalAlignment(SwingConstants.CENTER);
        personageAgeInput.setCaretColor(Color.white);
        personageAgeInput.setPreferredSize(new Dimension(200, 30));
        personageAgeInput.setEditable(false);

        String[] personages = new String[]{"Бакалавр", "Магистр", "Аспирант"};
        comboStudents = new JComboBox(personages);
        comboStudents.setSelectedIndex(0);
        comboStudents.setPreferredSize(new Dimension(200, 30));
        comboStudents.addItemListener(e -> {
            String personage = (String) e.getItem();
            switch (personage) {
                case "Бакалавр":
                    Student student = new Student("Никита Яковлев", "Бакалавр", 20);

                    BufferedImage bufferedPicture1 = null;
                    try {
                        bufferedPicture1 = ImageIO.read(new File("resourses/bacalavr_main.png"));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }

                    JLabel personageLabel = chiefWindowController.getCreateStudentWindow().getStudentLabel();

                    personageLabel.setIcon(new ImageIcon(bufferedPicture1));
                    chiefWindowController.getCreateStudentWindow().getStudentNameInput().setText(student.getName());
                    chiefWindowController.getCreateStudentWindow().getStudentAgeInput().setText(String.valueOf(student.getAge()));
                    break;
                case "Магистр":
                    bufferedPicture1 = null;

                    student = new Student("Снигерева Екатерина", "Магистр", 23);
                    try {
                        bufferedPicture1 = ImageIO.read(new File("resourses/magistrant_main.png"));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }

                    personageLabel = chiefWindowController.getCreateStudentWindow().getStudentLabel();
                    personageLabel.setIcon(new ImageIcon(bufferedPicture1));
                    chiefWindowController.getCreateStudentWindow().getStudentNameInput().setText(student.getName());
                    chiefWindowController.getCreateStudentWindow().getStudentAgeInput().setText(String.valueOf(student.getAge()));
                    break;
                case "Аспирант":
                    bufferedPicture1 = null;
                    student = new Student("Александр Дудаев", "Аспирант", 26);

                    try {
                        bufferedPicture1 = ImageIO.read(new File("resourses/aspirant_main.png"));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }

                    personageLabel = chiefWindowController.getCreateStudentWindow().getStudentLabel();
                    personageLabel.setIcon(new ImageIcon(bufferedPicture1));
                    chiefWindowController.getCreateStudentWindow().getStudentNameInput().setText(student.getName());
                    chiefWindowController.getCreateStudentWindow().getStudentAgeInput().setText(String.valueOf(student.getAge()));
                    break;
            }
        });

        JButton okaySelectStudentButton = new JButton("Ок");
        okaySelectStudentButton.setActionCommand("okaySelectStudentButtonPressed");
        okaySelectStudentButton.addActionListener(e -> {
            chiefWindowController.getWelcomeWindow().setNewStudentFlag();
            if (chiefWindowController.getWelcomeWindow().getNewStudentFlag() && chiefWindowController.getWelcomeWindow().getEnterOwner()) {
                chiefWindowController.getWelcomeWindow().getStartGameButton().setEnabled(true);
            }

            String personageName1 = chiefWindowController.getCreateStudentWindow().getStudentName();
            String personageType1 = chiefWindowController.getCreateStudentWindow().getStudentType();
            int personageAge1 = chiefWindowController.getCreateStudentWindow().getStudentAge();

            chiefWindowController.getPersonageModelController().getPersonageModel().setStudent(personageName1, personageType1, personageAge1);

            chiefWindowController.getCreateStudentWindow().getCreateStudentFrame().setVisible(false);
        });

        JPanel selectStudentInputPanel = new JPanel();
        selectStudentInputPanel.setBackground(Color.white);
        selectStudentInputPanel.setLayout(new FlowLayout());
        selectStudentInputPanel.setPreferredSize(new Dimension(200, 185));
        selectStudentInputPanel.add(personageName);
        selectStudentInputPanel.add(personageNameInput);
        selectStudentInputPanel.add(personageAge);
        selectStudentInputPanel.add(personageAgeInput);
        selectStudentInputPanel.add(personageType);
        selectStudentInputPanel.add(comboStudents);

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setBackground(Color.white);
        buttonsPanel.setLayout(new FlowLayout());
        buttonsPanel.add(okaySelectStudentButton);

        createStudentFrame.setLayout(new FlowLayout());
        createStudentFrame.setSize(280, 550);

        createStudentFrame.add(personageLabel);
        createStudentFrame.add(selectStudentInputPanel);
        createStudentFrame.add(buttonsPanel);
        createStudentFrame.setIconImage(new ImageIcon("resourses/icon.png").getImage());
        createStudentFrame.setLocationRelativeTo(null);
        createStudentFrame.setVisible(true);
    }


}
