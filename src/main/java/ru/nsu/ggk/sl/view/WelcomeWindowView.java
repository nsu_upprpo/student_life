package ru.nsu.ggk.sl.view;

import ru.nsu.ggk.sl.controller.ChiefWindowController;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class WelcomeWindowView extends JFrame {
    private boolean newStudentFlag;
    private boolean enterOwnerFlag;
    private JButton startGameButton;
    private JFrame welcomeFrame;


    void setEnterOwnerFlag() {
        enterOwnerFlag = true;
    }

    void setNewStudentFlag() {
        newStudentFlag = true;
    }

    boolean getNewStudentFlag() {
        return newStudentFlag;
    }

    boolean getEnterOwner() {
        return enterOwnerFlag;
    }

    JButton getStartGameButton() {
        return startGameButton;
    }

    public void createWelcomeWindow(ChiefWindowController chiefWindowController) {
        welcomeFrame = new JFrame("Начало");
        welcomeFrame.getContentPane().setBackground(Color.white);

        JButton newStudentButton = new JButton("Выбрать студента");
        newStudentButton.addActionListener(e -> chiefWindowController.startCreateStudentWindow());

        JButton ownerInfoButton = new JButton("Ввести данные владельца");
        ownerInfoButton.addActionListener(e -> chiefWindowController.startEnterOwnerWindow());

        startGameButton = new JButton("Начать игру Student's life");
        startGameButton.setEnabled(false);
        startGameButton.addActionListener(e -> chiefWindowController.startGameWindow());

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setBackground(Color.white);
        buttonsPanel.setLayout(new FlowLayout());
        buttonsPanel.add(newStudentButton);
        buttonsPanel.add(ownerInfoButton);
        buttonsPanel.add(startGameButton);
        buttonsPanel.setPreferredSize(new Dimension(300, 400));

        welcomeFrame.setLayout(new FlowLayout());
        welcomeFrame.setSize(360, 500);
        welcomeFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        BufferedImage bufferedPicture = null;
        try {
            bufferedPicture = ImageIO.read(new File("resourses/main.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        JLabel picLabel = new JLabel(new ImageIcon(bufferedPicture));

        welcomeFrame.add(picLabel);
        welcomeFrame.add(buttonsPanel);

        welcomeFrame.setIconImage(new ImageIcon("resourses/icon.png").getImage());
        welcomeFrame.setLocationRelativeTo(null);
        welcomeFrame.setVisible(true);

    }

    public WelcomeWindowView()  {
        newStudentFlag = false;
        enterOwnerFlag = false;
    }

}
