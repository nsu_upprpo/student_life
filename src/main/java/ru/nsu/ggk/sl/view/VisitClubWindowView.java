package ru.nsu.ggk.sl.view;

import ru.nsu.ggk.sl.controller.ChiefWindowController;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class VisitClubWindowView extends JFrame {
    private JFrame visitClubFrame;
    private JLabel studentPicture;
    private JButton buyTicketButton;
    private JButton payForTicketButton;

    private JLabel getstudentPicture() {
        return studentPicture;
    }

    private JFrame getVisitClubFrame() {
        return visitClubFrame;
    }

    private JButton getBuyTicketButton() {
        return buyTicketButton;
    }

    private JButton getPayForTicketButton() {
        return payForTicketButton;
    }

    public void createVisitClubWindow(ChiefWindowController chiefWindowController) {
        visitClubFrame = new JFrame("Сходить потусить");
        visitClubFrame.getContentPane().setBackground(Color.white);

        BufferedImage bufferedPicture = null;
        try {
            switch (chiefWindowController.getPersonageModelController().getPersonageModel().getStudentType())
            {
                case "Бакалавр":
                    bufferedPicture = ImageIO.read(new File("resourses/bacalavr_wait.png"));
                    break;
                case "Магистр":
                    bufferedPicture = ImageIO.read(new File("resourses/magistrant_wait.png"));
                    break;
                case "Аспирант":
                    bufferedPicture = ImageIO.read(new File("resourses/aspirant_wait.png"));
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        studentPicture = new JLabel(new ImageIcon(bufferedPicture));

        JButton returnHomeVisitClubButton = new JButton("Вернуться домой");
        returnHomeVisitClubButton.addActionListener(e -> {
            chiefWindowController.getVisitClubWindow().getVisitClubFrame().setVisible(false);
        });

        buyTicketButton = new JButton("Потанцевать в Бункере");
        buyTicketButton.setEnabled(false);
        buyTicketButton.addActionListener(e -> {
            switch (chiefWindowController.getPersonageModelController().getPersonageModel().getStudentType()) {
                case "Бакалавр":
                    try {
                        BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/bacalavr_victory.png"));
                        chiefWindowController.getVisitClubWindow().getstudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
                case "Магистр":
                    try {
                        BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/magistrant_victory.png"));
                        chiefWindowController.getVisitClubWindow().getstudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
                case "Аспирант":
                    try {
                        BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/aspirant_victory.png"));
                        chiefWindowController.getVisitClubWindow().getstudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
                    }


            int moodState = 100;
            chiefWindowController.getPersonageModelController().getPersonageModel().setMoodState(moodState);
            chiefWindowController.getPersonageModelController().getModelPropertyChange().firePropertyChange("MoodStateProperty", 0, 1);
            chiefWindowController.getVisitClubWindow().getBuyTicketButton().setEnabled(false);
            chiefWindowController.getVisitClubWindow().getPayForTicketButton().setEnabled(true);
        });

        payForTicketButton = new JButton("Заплатить за вход в бункер");
        payForTicketButton.addActionListener(e -> {
            switch (chiefWindowController.getPersonageModelController().getPersonageModel().getStudentType()) {
                case "Бакалавр":
                    try {
                        BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/bacalavr_cool.png"));
                        chiefWindowController.getVisitClubWindow().getstudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
                case "Магистр":
                    try {
                        BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/magistrant_cool.png"));
                        chiefWindowController.getVisitClubWindow().getstudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
                case "Аспирант":
                    try {
                        BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/aspirant_cool.png"));
                        chiefWindowController.getVisitClubWindow().getstudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
            }

            int coinsAmount = chiefWindowController.getPersonageModelController().getPersonageModel().getCoinsAmount();
            if (coinsAmount - 2 < 0) {
                return;
            }
            coinsAmount -= 2;
            chiefWindowController.getPersonageModelController().getPersonageModel().setCoinsAmount(coinsAmount);
            chiefWindowController.getPersonageModelController().getModelPropertyChange().firePropertyChange("OwnerProperty", 0, 1);

            chiefWindowController.getVisitClubWindow().getBuyTicketButton().setEnabled(true);
            chiefWindowController.getVisitClubWindow().getPayForTicketButton().setEnabled(false);
        });

        payForTicketButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        buyTicketButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        returnHomeVisitClubButton.setAlignmentX(Component.CENTER_ALIGNMENT);

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setBackground(Color.white);
        buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.Y_AXIS));
        buttonsPanel.add(payForTicketButton);
        buttonsPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        buttonsPanel.add(buyTicketButton);
        buttonsPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        buttonsPanel.add(returnHomeVisitClubButton);

        visitClubFrame.setLayout(new FlowLayout());
        visitClubFrame.setSize(450, 260);

        visitClubFrame.add(studentPicture);
        visitClubFrame.add(buttonsPanel);
        visitClubFrame.setIconImage(new ImageIcon("resourses/icon.png").getImage());
        visitClubFrame.setLocationRelativeTo(null);
        visitClubFrame.setVisible(true);
    }

}
