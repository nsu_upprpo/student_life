package ru.nsu.ggk.sl.view;

import ru.nsu.ggk.sl.controller.ChiefWindowController;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class EarnMoneyWindowView extends JFrame {
    private JFrame earnMoneyFrame;
    private JLabel studentPicture;

    private JLabel getStudentPicture() {
        return studentPicture;
    }

    private JFrame getEarnMoneyFrame() {
        return earnMoneyFrame;
    }

    public void createEarnMoneyWindow(ChiefWindowController chiefWindowController) {
        earnMoneyFrame = new JFrame("Заработок денег");
        earnMoneyFrame.getContentPane().setBackground(Color.white);

        BufferedImage bufferedPicture = null;
        try {
            switch (chiefWindowController.getPersonageModelController().getPersonageModel().getStudentType())
            {
                case "Бакалавр":
                    bufferedPicture = ImageIO.read(new File("resourses/bacalavr_wait.png"));
                    break;
                case "Магистр":
                    bufferedPicture = ImageIO.read(new File("resourses/magistrant_wait.png"));
                    break;
                case "Аспирант":
                    bufferedPicture = ImageIO.read(new File("resourses/aspirant_wait.png"));
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        studentPicture = new JLabel(new ImageIcon(bufferedPicture));

        JButton goWorkButton = new JButton("Подработать репетитором");
        goWorkButton.addActionListener(e -> {
            switch (chiefWindowController.getPersonageModelController().getPersonageModel().getStudentType()) {
                case "Бакалавр":
                    try {
                        BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/bacalavr_collect.png"));
                        chiefWindowController.getEarnMoneyWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
                case "Магистр":
                    try {
                        BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/magistrant_collect.png"));
                        chiefWindowController.getEarnMoneyWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
                case "Аспирант":
                    try {
                        BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/aspirant_collect.png"));
                        chiefWindowController.getEarnMoneyWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
            }

            int coinsAmount = chiefWindowController.getPersonageModelController().getPersonageModel().getCoinsAmount();
            coinsAmount += 50;
            chiefWindowController.getPersonageModelController().getPersonageModel().setCoinsAmount(coinsAmount);
            chiefWindowController.getPersonageModelController().getModelPropertyChange().firePropertyChange("OwnerProperty", 0, 1);
        });

        JButton sellSomethingButton = new JButton("Пойти на работу");
        sellSomethingButton.addActionListener(e -> {
            switch (chiefWindowController.getPersonageModelController().getPersonageModel().getStudentType()) {
                case "Бакалавр":
                    try {
                        BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/bacalavr_collect.png"));
                        chiefWindowController.getEarnMoneyWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
                case "Магистр":
                    try {
                        BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/magistrant_collect.png"));
                        chiefWindowController.getEarnMoneyWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;

                case "Аспирант":
                    try {
                        BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/aspirant_collect.png"));
                        chiefWindowController.getEarnMoneyWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
        }

            int coinsAmount = chiefWindowController.getPersonageModelController().getPersonageModel().getCoinsAmount();
            coinsAmount += 20;
            chiefWindowController.getPersonageModelController().getPersonageModel().setCoinsAmount(coinsAmount);
            chiefWindowController.getPersonageModelController().getModelPropertyChange().firePropertyChange("OwnerProperty", 0, 1);
        });

        JButton askPeopleForMoneyButton = new JButton("Попросить у родителей");
        askPeopleForMoneyButton.addActionListener(e -> {switch (chiefWindowController.getPersonageModelController().getPersonageModel().getStudentType()) {
            case "Бакалавр":
                try {
                    BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/bacalavr_collect.png"));
                    chiefWindowController.getEarnMoneyWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
                break;
            case "Магистр":
                try {
                    BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/magistrant_collect.png"));
                    chiefWindowController.getEarnMoneyWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
                break;

            case "Аспирант":
                try {
                    BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/aspirant_collect.png"));
                    chiefWindowController.getEarnMoneyWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
                break;
        }

            int coinsAmount = chiefWindowController.getPersonageModelController().getPersonageModel().getCoinsAmount();

            if ((int) (Math.random() * 2) == 1) {
                coinsAmount++;
                chiefWindowController.getPersonageModelController().getPersonageModel().setCoinsAmount(coinsAmount);
                chiefWindowController.getPersonageModelController().getModelPropertyChange().firePropertyChange("OwnerProperty", 0, 1);
            }
        });

        JButton okayEarnMoneyButton = new JButton("Закончить заработок");
        okayEarnMoneyButton.addActionListener(e -> {
            chiefWindowController.getEarnMoneyWindow().getEarnMoneyFrame().setVisible(false);
        });

        goWorkButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        sellSomethingButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        askPeopleForMoneyButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        okayEarnMoneyButton.setAlignmentX(Component.CENTER_ALIGNMENT);

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setBackground(Color.white);
        buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.Y_AXIS));

        buttonsPanel.add(goWorkButton);
        buttonsPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        buttonsPanel.add(sellSomethingButton);
        buttonsPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        buttonsPanel.add(askPeopleForMoneyButton);
        buttonsPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        buttonsPanel.add(okayEarnMoneyButton);

        earnMoneyFrame.setLayout(new FlowLayout());
        earnMoneyFrame.setSize(500, 270);

        earnMoneyFrame.add(studentPicture);
        earnMoneyFrame.add(buttonsPanel);
        earnMoneyFrame.setIconImage(new ImageIcon("resourses/icon.png").getImage());
        earnMoneyFrame.setLocationRelativeTo(null);
        earnMoneyFrame.setVisible(true);
    }

}
