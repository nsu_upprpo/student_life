package ru.nsu.ggk.sl.view;

import ru.nsu.ggk.sl.controller.ChiefWindowController;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class GameWindowView extends JFrame {
    private JFrame gameFrame;
    private JLabel coinsAmount;
    private JLabel foodAmount;
    private JLabel waterAmount;
    private JLabel ownerName;
    private JLabel ownerAge;

    private JLabel studentName;
    private JLabel studentType;
    private JLabel studentAge;

    private JProgressBar satietyBar;
    private JProgressBar achievementBar;
    private JProgressBar vivacityBar;
    private JProgressBar moodBar;

    public JProgressBar getMoodBar() {
        return moodBar;
    }

    public JProgressBar getSatietyBar() {
        return satietyBar;
    }

    public JProgressBar getAchievementBar() {
        return achievementBar;
    }

    public JProgressBar getVivacityBar() {
        return vivacityBar;
    }

    public JLabel getOwnerName() {
        return ownerName;
    }

    public JLabel getOwnerAge() {
        return ownerAge;
    }

    public JLabel getCoinsAmount() {
        return coinsAmount;
    }

    public JLabel getFoodAmount() {
        return foodAmount;
    }

    public JLabel getWaterAmount() {
        return waterAmount;
    }

    public void createGameWindow(ChiefWindowController chiefWindowController) {
        gameFrame = new JFrame("Student's life");
        gameFrame.getContentPane().setBackground(Color.white);

        Font font = new Font(new JLabel("").getFont().getName(), Font.PLAIN, 14);

        JLabel ownerNameLabel = new JLabel("Имя игрока:");
        ownerNameLabel.setFont(font.deriveFont(font.getStyle() | Font.BOLD));
        ownerName = new JLabel("");
        JLabel coinsAmountLabel = new JLabel("Количество денег:");
        coinsAmountLabel.setFont(font.deriveFont(font.getStyle() | Font.BOLD));
        coinsAmount = new JLabel("");
        JLabel ownerAgeLabel = new JLabel("Пол игрока:");
        ownerAgeLabel.setFont(font.deriveFont(font.getStyle() | Font.BOLD));
        ownerAge = new JLabel("");
        chiefWindowController.getPersonageModelController().getPersonageModel().setOwner(3000);
        JLabel foodAmountLabel = new JLabel("Количество продуктов:");
        foodAmountLabel.setFont(font.deriveFont(font.getStyle() | Font.BOLD));
        foodAmount = new JLabel("");
        JLabel waterAmountLabel = new JLabel("Количество еды из столовой:");
        waterAmountLabel.setFont(font.deriveFont(font.getStyle() | Font.BOLD));
        waterAmount = new JLabel("");

        JLabel studentNameLabel = new JLabel("Имя студента:");
        studentNameLabel.setFont(font.deriveFont(font.getStyle() | Font.BOLD));
        studentName = new JLabel("");
        JLabel studentTypeLabel = new JLabel("Вид студента:");
        studentTypeLabel.setFont(font.deriveFont(font.getStyle() | Font.BOLD));
        studentType = new JLabel("");
        JLabel studentAgeLabel = new JLabel("Возраст студента:");
        studentAgeLabel.setFont(font.deriveFont(font.getStyle() | Font.BOLD));
        studentAge = new JLabel("");

        JLabel studentLabel = new JLabel("Студент: ");
        studentLabel.setFont(font.deriveFont(font.getStyle() | Font.BOLD));

        JPanel gameInfoPanel = new JPanel();
        gameInfoPanel.setBackground(Color.white);
        gameInfoPanel.setLayout(new BoxLayout(gameInfoPanel, BoxLayout.Y_AXIS));
        gameInfoPanel.setPreferredSize(new Dimension(250, 690));

        gameInfoPanel.setBorder(BorderFactory.createTitledBorder("ИГРОВЫЕ ДАННЫЕ"));
        gameInfoPanel.add(Box.createRigidArea(new Dimension(20, 10)));

        ownerNameLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        ownerAgeLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        coinsAmountLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        coinsAmount.setAlignmentX(Component.LEFT_ALIGNMENT);
        ownerName.setAlignmentX(Component.LEFT_ALIGNMENT);
        ownerAge.setAlignmentX(Component.LEFT_ALIGNMENT);
        studentNameLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        studentName.setAlignmentX(Component.LEFT_ALIGNMENT);
        studentLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        studentAgeLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        studentTypeLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        studentAge.setAlignmentX(Component.LEFT_ALIGNMENT);
        studentType.setAlignmentX(Component.LEFT_ALIGNMENT);

        BufferedImage bufferedPicture = null;

        try {
            switch (chiefWindowController.getPersonageModelController().getPersonageModel().getStudentType()) {
                case "Бакалавр":
                    bufferedPicture = ImageIO.read(new File("resourses/bacalavr_main.png"));
                    break;
                case "Магистр":
                    bufferedPicture = ImageIO.read(new File("resourses/magistrant_main.png"));
                    break;
                case "Аспирант":
                    bufferedPicture = ImageIO.read(new File("resourses/aspirant_main.png"));
                    break;
            }
        }catch (IOException e)
        {
            e.printStackTrace();
        }

        JLabel studentPicture = new JLabel(new ImageIcon(bufferedPicture));

        gameInfoPanel.add(ownerNameLabel);
        gameInfoPanel.add(ownerName);
        gameInfoPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        gameInfoPanel.add(ownerAgeLabel);
        gameInfoPanel.add(ownerAge);
        gameInfoPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        gameInfoPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        gameInfoPanel.add(coinsAmountLabel);
        gameInfoPanel.add(coinsAmount);

        gameInfoPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        gameInfoPanel.add(foodAmountLabel);
        gameInfoPanel.add(foodAmount);
        gameInfoPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        gameInfoPanel.add(waterAmountLabel);
        gameInfoPanel.add(waterAmount);

        gameInfoPanel.add(Box.createRigidArea(new Dimension(0, 30)));
        gameInfoPanel.add(studentNameLabel);
        gameInfoPanel.add(studentName);
        gameInfoPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        gameInfoPanel.add(studentTypeLabel);
        gameInfoPanel.add(studentType);
        gameInfoPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        gameInfoPanel.add(studentAgeLabel);
        gameInfoPanel.add(studentAge);
        gameInfoPanel.add(Box.createRigidArea(new Dimension(0, 30)));
        gameInfoPanel.add(studentLabel);
        gameInfoPanel.add(Box.createRigidArea(new Dimension(0, 20)));
        gameInfoPanel.add(studentPicture);

        JButton studyStudentButton = new JButton("Учиться");
        studyStudentButton.addActionListener(e -> {
            chiefWindowController.startStudyStudentWindow();
        });

        JButton visitClubButton = new JButton("Сходить потусить");
        visitClubButton.addActionListener(e -> {
            chiefWindowController.startVisitClubWindow();
        });

        JButton visitShopButton = new JButton("Пойти за едой");
        visitShopButton.addActionListener(e -> {
            chiefWindowController.startVisitShopWindow();
        });

        JButton feedStudentButton = new JButton("Поесть");
        feedStudentButton.addActionListener(e -> {
            chiefWindowController.startFeedStudentWindow();
        });

        JButton earnMoneyButton = new JButton("Заработать деньги");
        earnMoneyButton.addActionListener(e -> {
            chiefWindowController.startEarnMoneyWindow();
        });

        JButton visitConferenceButton = new JButton("Посетить конференцию");
        visitConferenceButton.addActionListener(e -> {
            chiefWindowController.startVisitConferenceWindow();
        });

        JButton sleepStudentButton = new JButton("Лечь спать");
        sleepStudentButton.addActionListener(e -> {
            chiefWindowController.getPersonageModelController().getPersonageModel().setVivacityState(100);
            chiefWindowController.getPersonageModelController().getModelPropertyChange().firePropertyChange("VivacityStateProperty", 0, 1);
        });

        studyStudentButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        visitClubButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        visitShopButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        feedStudentButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        earnMoneyButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        visitConferenceButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        sleepStudentButton.setAlignmentX(Component.CENTER_ALIGNMENT);

        JPanel gameActionsButtonsPanel = new JPanel();
        gameActionsButtonsPanel.setBackground(Color.white);
        gameActionsButtonsPanel.setLayout(new BoxLayout(gameActionsButtonsPanel, BoxLayout.Y_AXIS));
        gameActionsButtonsPanel.setPreferredSize(new Dimension(250, 690));
        gameActionsButtonsPanel.setBorder(BorderFactory.createTitledBorder("ИГРОВЫЕ ДЕЙСТВИЯ"));

        gameActionsButtonsPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        gameActionsButtonsPanel.add(studyStudentButton);
        gameActionsButtonsPanel.add(Box.createRigidArea(new Dimension(0, 20)));
        gameActionsButtonsPanel.add(visitClubButton);

        gameActionsButtonsPanel.add(Box.createRigidArea(new Dimension(0, 20)));
        gameActionsButtonsPanel.add(visitShopButton);

        gameActionsButtonsPanel.add(Box.createRigidArea(new Dimension(0, 20)));
        gameActionsButtonsPanel.add(feedStudentButton);

        gameActionsButtonsPanel.add(Box.createRigidArea(new Dimension(0, 20)));
        gameActionsButtonsPanel.add(earnMoneyButton);

        gameActionsButtonsPanel.add(Box.createRigidArea(new Dimension(0, 20)));
        gameActionsButtonsPanel.add(visitConferenceButton);

        gameActionsButtonsPanel.add(Box.createRigidArea(new Dimension(0, 20)));
        gameActionsButtonsPanel.add(sleepStudentButton);


        JLabel satietyLabel = new JLabel("Сытость:");
        JLabel achievementLabel = new JLabel("Успеваемость");
        JLabel vivacityLabel = new JLabel("Бодрость:");
        JLabel moodLabel = new JLabel("Настроение:");

        moodLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        satietyLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        vivacityLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
        achievementLabel.setAlignmentX(Component.LEFT_ALIGNMENT);

        moodBar = new JProgressBar();
        moodBar.setMaximum(100);
        moodBar.setMinimum(0);
        moodBar.setValue(20);

        satietyBar = new JProgressBar();
        satietyBar.setMaximum(100);
        satietyBar.setMinimum(0);
        satietyBar.setValue(50);

        achievementBar = new JProgressBar();
        achievementBar.setMaximum(100);
        achievementBar.setMinimum(0);
        achievementBar.setValue(90);

        vivacityBar = new JProgressBar();
        vivacityBar.setMaximum(100);
        vivacityBar.setMinimum(0);
        vivacityBar.setValue(20);

        JPanel personageStatePanel = new JPanel();
        personageStatePanel.setBackground(Color.white);
        personageStatePanel.setLayout(new BoxLayout(personageStatePanel, BoxLayout.Y_AXIS));
        personageStatePanel.setPreferredSize(new Dimension(250, 690));
        personageStatePanel.setBorder(BorderFactory.createTitledBorder("СОСТОЯНИЕ ПИТОМЦА"));

        personageStatePanel.add(Box.createRigidArea(new Dimension(0, 20)));
        personageStatePanel.add(satietyLabel);
        personageStatePanel.add(satietyBar);
        personageStatePanel.add(Box.createRigidArea(new Dimension(0, 20)));
        personageStatePanel.add(achievementLabel);
        personageStatePanel.add(achievementBar);
        personageStatePanel.add(Box.createRigidArea(new Dimension(0, 20)));
        personageStatePanel.add(vivacityLabel);
        personageStatePanel.add(vivacityBar);
        personageStatePanel.add(Box.createRigidArea(new Dimension(0, 10)));
        personageStatePanel.add(moodLabel);
        personageStatePanel.add(moodBar);

        gameFrame.setLayout(new FlowLayout());
        gameFrame.setSize(780, 730);
        gameFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        gameFrame.add(gameInfoPanel);
        gameFrame.add(gameActionsButtonsPanel);
        gameFrame.add(personageStatePanel);
        gameFrame.setIconImage(new ImageIcon("resourses/icon.png").getImage());
        gameFrame.setLocationRelativeTo(null);
        gameFrame.setVisible(true);
    }

    public JLabel getStudentName() {
        return studentName;
    }

    public JLabel getStudentType() {
        return studentType;
    }

    public JLabel getStudentAge() {
        return studentAge;
    }
}