package ru.nsu.ggk.sl.view;

import ru.nsu.ggk.sl.controller.ChiefWindowController;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class StudyStudentWindowView extends JFrame {
    private JFrame StudyStudentFrame;
    private JLabel studentPicture;

    private JLabel getStudentPicture() {
        return studentPicture;
    }

    private JFrame getStudyStudentFrame() {
        return StudyStudentFrame;
    }
    public void createStudyStudentWindow(ChiefWindowController chiefWindowController) {
        StudyStudentFrame = new JFrame("Учёба");
        StudyStudentFrame.getContentPane().setBackground(Color.white);

        JButton okayStudyStudentButton = new JButton("Закончить учиться");
        okayStudyStudentButton.addActionListener(e -> {
            chiefWindowController.getStudyStudentWindow().getStudyStudentFrame().setVisible(false);
        });

        JButton homeWorkButton = new JButton("Позаниматься дома");
        homeWorkButton.addActionListener(e -> {
            switch (chiefWindowController.getPersonageModelController().getPersonageModel().getStudentType()) {
                case "Бакалавр":
                    try {
                        BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/bacalavr_collect.png"));
                        chiefWindowController.getStudyStudentWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
                case "Магистр":
                    try {
                        BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/magistrant_collect.png"));
                        chiefWindowController.getStudyStudentWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
                case "Аспирант":
                    try {
                        BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/aspirant_collect.png"));
                        chiefWindowController.getStudyStudentWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
            }


            int achievementState = chiefWindowController.getPersonageModelController().getPersonageModel().getAchievementState();
            achievementState = Math.min(achievementState + 10, 100);
            chiefWindowController.getPersonageModelController().getPersonageModel().setAchievementState(achievementState);
            chiefWindowController.getPersonageModelController().getModelPropertyChange().firePropertyChange("AchievementStateProperty", 0, 1);

            int vivacityState = chiefWindowController.getPersonageModelController().getPersonageModel().getVivacityState();
            vivacityState = Math.max(vivacityState - 10, 0);
            chiefWindowController.getPersonageModelController().getPersonageModel().setVivacityState(vivacityState);
            chiefWindowController.getPersonageModelController().getModelPropertyChange().firePropertyChange("VivacityStateProperty", 0, 1);

            int satietyState = chiefWindowController.getPersonageModelController().getPersonageModel().getSatietyState();
            satietyState = Math.max(satietyState - 2, 0);
            chiefWindowController.getPersonageModelController().getPersonageModel().setSatietyState(satietyState);
            chiefWindowController.getPersonageModelController().getModelPropertyChange().firePropertyChange("SatietyStateProperty", 0, 1);
        });

        JButton goToClassButton = new JButton("Пойти на пары");
        goToClassButton.addActionListener(e -> {
            switch (chiefWindowController.getPersonageModelController().getPersonageModel().getStudentType()) {
                case "Бакалавр":
                    try {
                        BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/bacalavr_run.png"));
                        chiefWindowController.getStudyStudentWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
                case "Магистр":
                    try {
                        BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/magistrant_run.png"));
                        chiefWindowController.getStudyStudentWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
                case "Аспирант":
                    try {
                        BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/aspirant_run.png"));
                        chiefWindowController.getStudyStudentWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
            }

            int achievementState = chiefWindowController.getPersonageModelController().getPersonageModel().getAchievementState();
            achievementState = Math.min(achievementState + 20, 100);
            chiefWindowController.getPersonageModelController().getPersonageModel().setAchievementState(achievementState);
            chiefWindowController.getPersonageModelController().getModelPropertyChange().firePropertyChange("AchievementStateProperty", 0, 1);

            int vivacityState = chiefWindowController.getPersonageModelController().getPersonageModel().getVivacityState();
            vivacityState = Math.max(vivacityState - 20, 0);
            chiefWindowController.getPersonageModelController().getPersonageModel().setVivacityState(vivacityState);
            chiefWindowController.getPersonageModelController().getModelPropertyChange().firePropertyChange("VivacityStateProperty", 0, 1);

            int satietyState = chiefWindowController.getPersonageModelController().getPersonageModel().getSatietyState();
            satietyState = Math.max(satietyState - 4, 0);
            chiefWindowController.getPersonageModelController().getPersonageModel().setSatietyState(satietyState);
            chiefWindowController.getPersonageModelController().getModelPropertyChange().firePropertyChange("SatietyStateProperty", 0, 1);
        });

        goToClassButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        homeWorkButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        okayStudyStudentButton.setAlignmentX(Component.CENTER_ALIGNMENT);

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setBackground(Color.white);
        buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.Y_AXIS));
        buttonsPanel.add( goToClassButton);
        buttonsPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        buttonsPanel.add(homeWorkButton);
        buttonsPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        buttonsPanel.add(okayStudyStudentButton);

        BufferedImage bufferedPicture = null;


        try {
            switch (chiefWindowController.getPersonageModelController().getPersonageModel().getStudentType())
            {
                case "Бакалавр":
                    bufferedPicture = ImageIO.read(new File("resourses/bacalavr_wait.png"));
                    break;
                case "Магистр":
                    bufferedPicture = ImageIO.read(new File("resourses/magistrant_wait.png"));
                    break;
                case "Аспирант":
                    bufferedPicture = ImageIO.read(new File("resourses/aspirant_wait.png"));
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        studentPicture = new JLabel(new ImageIcon(bufferedPicture));

        StudyStudentFrame.setLayout(new FlowLayout());
        StudyStudentFrame.setSize(430, 250);

        StudyStudentFrame.add(studentPicture);
        StudyStudentFrame.add(buttonsPanel);
        StudyStudentFrame.setIconImage(new ImageIcon("resourses/icon.png").getImage());
        StudyStudentFrame.setLocationRelativeTo(null);
        StudyStudentFrame.setVisible(true);
    }
}
