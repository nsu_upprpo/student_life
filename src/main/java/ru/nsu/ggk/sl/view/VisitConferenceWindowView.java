package ru.nsu.ggk.sl.view;

import ru.nsu.ggk.sl.controller.ChiefWindowController;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class VisitConferenceWindowView extends JFrame {
    private JFrame VisitConferenceFrame;
    private JLabel studentPicture;

    private JLabel getStudentPicture() {
        return studentPicture;
    }

    public void createVisitConferenceWindow(ChiefWindowController chiefWindowController) {
        VisitConferenceFrame = new JFrame("Посетить конференцию");
        VisitConferenceFrame.getContentPane().setBackground(Color.white);

        BufferedImage bufferedPicture = null;
        try {
            switch (chiefWindowController.getPersonageModelController().getPersonageModel().getStudentType())
            {
                case "Бакалавр":
                    bufferedPicture = ImageIO.read(new File("resourses/bacalavr_wait.png"));
                    break;
                case "Магистр":
                    bufferedPicture = ImageIO.read(new File("resourses/magistrant_wait.png"));
                    break;
                case "Аспирант":
                    bufferedPicture = ImageIO.read(new File("resourses/aspirant_wait.png"));
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        studentPicture = new JLabel(new ImageIcon(bufferedPicture));

        JButton makePresentationButton = new JButton("Выступить с докладом");
        makePresentationButton.addActionListener(e -> {
            switch (chiefWindowController.getPersonageModelController().getPersonageModel().getStudentType()) {
                case "Бакалавр":
                    try {
                        BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/bacalavr_enjoy.png"));
                        chiefWindowController.getVisitConferenceWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
                case "Магистр":
                    try {
                        BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/magistrant_enjoy.png"));
                        chiefWindowController.getVisitConferenceWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
                case "Аспирант":
                    try {
                        BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/aspirant_enjoy.png"));
                        chiefWindowController.getVisitConferenceWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
                    }

            int achievementState = chiefWindowController.getPersonageModelController().getPersonageModel().getAchievementState();
            achievementState = Math.min(achievementState + 50, 100);
            chiefWindowController.getPersonageModelController().getPersonageModel().setAchievementState(achievementState);
            chiefWindowController.getPersonageModelController().getModelPropertyChange().firePropertyChange("AchievementStateProperty", 0, 1);

            int vivacityState = chiefWindowController.getPersonageModelController().getPersonageModel().getVivacityState();
            vivacityState = Math.max(vivacityState - 10, 0);
            chiefWindowController.getPersonageModelController().getPersonageModel().setVivacityState(vivacityState);
            chiefWindowController.getPersonageModelController().getModelPropertyChange().firePropertyChange("VivacityStateProperty", 0, 1);

            int satietyState = chiefWindowController.getPersonageModelController().getPersonageModel().getSatietyState();
            satietyState = Math.max(satietyState - 2, 0);
            chiefWindowController.getPersonageModelController().getPersonageModel().setSatietyState(satietyState);
            chiefWindowController.getPersonageModelController().getModelPropertyChange().firePropertyChange("SatietyStateProperty", 0, 1);

            int moodState = chiefWindowController.getPersonageModelController().getPersonageModel().getMoodState();
            moodState = Math.min(moodState + 20, 100);
            chiefWindowController.getPersonageModelController().getPersonageModel().setMoodState(moodState);
            chiefWindowController.getPersonageModelController().getModelPropertyChange().firePropertyChange("MoodStateProperty", 0, 1);
        });

        JButton listenPresentationButton = new JButton("Послушать доклад");
        listenPresentationButton.addActionListener(e -> {
            switch (chiefWindowController.getPersonageModelController().getPersonageModel().getStudentType()) {
                case "Бакалавр":
                    try {
                        BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/bacalavr_enjoy.png"));
                        chiefWindowController.getVisitConferenceWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
                case "Магистр":
                    try {
                        BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/magistrant_enjoy.png"));
                        chiefWindowController.getVisitConferenceWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
                case "Аспирант":
                    try {
                        BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/aspirant_enjoy.png"));
                        chiefWindowController.getVisitConferenceWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
            }

            int achievementState = chiefWindowController.getPersonageModelController().getPersonageModel().getAchievementState();
            achievementState = Math.min(achievementState + 25, 100);
            chiefWindowController.getPersonageModelController().getPersonageModel().setAchievementState(achievementState);
            chiefWindowController.getPersonageModelController().getModelPropertyChange().firePropertyChange("AchievementStateProperty", 0, 1);

            int vivacityState = chiefWindowController.getPersonageModelController().getPersonageModel().getVivacityState();
            vivacityState = Math.max(vivacityState - 25, 0);
            chiefWindowController.getPersonageModelController().getPersonageModel().setVivacityState(vivacityState);
            chiefWindowController.getPersonageModelController().getModelPropertyChange().firePropertyChange("VivacityStateProperty", 0, 1);

            int satietyState = chiefWindowController.getPersonageModelController().getPersonageModel().getSatietyState();
            satietyState = Math.max(satietyState - 4, 0);
            chiefWindowController.getPersonageModelController().getPersonageModel().setSatietyState(satietyState);
            chiefWindowController.getPersonageModelController().getModelPropertyChange().firePropertyChange("SatietyStateProperty", 0, 1);

            int moodState = chiefWindowController.getPersonageModelController().getPersonageModel().getMoodState();
            moodState = Math.min(moodState + 30, 100);
            chiefWindowController.getPersonageModelController().getPersonageModel().setMoodState(moodState);
            chiefWindowController.getPersonageModelController().getModelPropertyChange().firePropertyChange("MoodStateProperty", 0, 1);
        });

        JButton returnHomeConferenceButton = new JButton("Вернуться домой");
        returnHomeConferenceButton.addActionListener(e -> VisitConferenceFrame.setVisible(false));

        makePresentationButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        listenPresentationButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        returnHomeConferenceButton.setAlignmentX(Component.CENTER_ALIGNMENT);

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setBackground(Color.white);
        buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.Y_AXIS));
        buttonsPanel.add(makePresentationButton);
        buttonsPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        buttonsPanel.add(listenPresentationButton);
        buttonsPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        buttonsPanel.add(returnHomeConferenceButton);

        VisitConferenceFrame.setLayout(new FlowLayout());
        VisitConferenceFrame.setSize(440, 240);

        VisitConferenceFrame.add(studentPicture);
        VisitConferenceFrame.add(buttonsPanel);
        VisitConferenceFrame.setIconImage(new ImageIcon("resourses/icon.png").getImage());
        VisitConferenceFrame.setLocationRelativeTo(null);
        VisitConferenceFrame.setVisible(true);
    }
}
