package ru.nsu.ggk.sl.view;

import ru.nsu.ggk.sl.controller.ChiefWindowController;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class FeedStudentWindowView extends JFrame {
    private JFrame feedStudentFrame;
    private JLabel studentPicture;

    private JLabel getStudentPicture() {
        return studentPicture;
    }

    private JFrame getFeedStudentFrame() {
        return feedStudentFrame;
    }

    public void createWalkStudentWindow(ChiefWindowController chiefWindowController) {
        feedStudentFrame = new JFrame("Кормление питомца");
        feedStudentFrame.getContentPane().setBackground(Color.GRAY);

        BufferedImage bufferedPicture = null;

        try {
            switch (chiefWindowController.getPersonageModelController().getPersonageModel().getStudentType())
            {
                case "Бакалавр":
                    bufferedPicture = ImageIO.read(new File("resourses/bacalavr_wait.png"));
                    break;
                case "Магистр":
                    bufferedPicture = ImageIO.read(new File("resourses/magistrant_wait.png"));
                    break;
                case "Аспирант":
                    bufferedPicture = ImageIO.read(new File("resourses/aspirant_wait.png"));
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        studentPicture = new JLabel(new ImageIcon(bufferedPicture));

        JButton okayFeedStudentButton = new JButton("Закончить приём пищи");
        okayFeedStudentButton.addActionListener(e -> {
            chiefWindowController.getFeedStudentWindow().getFeedStudentFrame().setVisible(false);
        });

        JButton feedFoodButton = new JButton("Приготовить продукты и поесть");
        feedFoodButton.addActionListener(e -> {
            switch (chiefWindowController.getPersonageModelController().getPersonageModel().getStudentType()) {
                case "Бакалавр":
                    try {
                        BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/bacalavr_love.png"));
                        chiefWindowController.getFeedStudentWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                    } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
                    break;
                case "Магистр":
                    try {
                        BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/magistrant_love.png"));
                        chiefWindowController.getFeedStudentWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
                case "Аспирант":
                    try {
                        BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/aspirant_love.png"));
                        chiefWindowController.getFeedStudentWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
        }

            int foodAmount = chiefWindowController.getPersonageModelController().getPersonageModel().getFoodAmount();
            if (foodAmount > 0) {
                int satietyState = chiefWindowController.getPersonageModelController().getPersonageModel().getSatietyState();
                satietyState = Math.min(satietyState + 50, 100);
                chiefWindowController.getPersonageModelController().getPersonageModel().setSatietyState(satietyState);
                chiefWindowController.getPersonageModelController().getPersonageModel().setFoodAmount(--foodAmount);
                chiefWindowController.getPersonageModelController().getModelPropertyChange().firePropertyChange("OwnerProperty", 0, 1);
                chiefWindowController.getPersonageModelController().getModelPropertyChange().firePropertyChange("SatietyStateProperty", 0, 1);
            }
        });

        JButton feedDrinkButton = new JButton("Поесть еду в столовой");

        feedDrinkButton.addActionListener(e -> {
                switch (chiefWindowController.getPersonageModelController().getPersonageModel().getStudentType()) {
                    case "Бакалавр":
                        try {
                            BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/bacalavr_love.png"));
                            chiefWindowController.getFeedStudentWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                        } catch (IOException ex) {
                            System.out.println(ex.getMessage());
                        }
                        break;
                    case "Магистр":
                        try {
                            BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/magistrant_love.png"));
                            chiefWindowController.getFeedStudentWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                        } catch (IOException ex) {
                            System.out.println(ex.getMessage());
                        }
                        break;
                    case "Аспирант":
                        try {
                            BufferedImage bufferedPicture1 = ImageIO.read(new File("resourses/aspirant_love.png"));
                            chiefWindowController.getFeedStudentWindow().getStudentPicture().setIcon(new ImageIcon(bufferedPicture1));
                        } catch (IOException ex) {
                            System.out.println(ex.getMessage());
                        }
                        break;
                }

                int drinkAmount = chiefWindowController.getPersonageModelController().getPersonageModel().getDrinkAmount();
            if (drinkAmount > 0) {
                int satietyState = chiefWindowController.getPersonageModelController().getPersonageModel().getSatietyState();
                satietyState = Math.min(satietyState + 25, 100);
                chiefWindowController.getPersonageModelController().getPersonageModel().setSatietyState(satietyState);
                chiefWindowController.getPersonageModelController().getPersonageModel().setDrinkAmount(--drinkAmount);
                chiefWindowController.getPersonageModelController().getModelPropertyChange().firePropertyChange("OwnerProperty", 0, 1);
                chiefWindowController.getPersonageModelController().getModelPropertyChange().firePropertyChange("SatietyStateProperty", 0, 1);
            }
        });

        feedFoodButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        feedDrinkButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        okayFeedStudentButton.setAlignmentX(Component.CENTER_ALIGNMENT);

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setBackground(Color.white);
        buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.Y_AXIS));

        buttonsPanel.add(feedDrinkButton);
        buttonsPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        buttonsPanel.add(feedFoodButton);
        buttonsPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        buttonsPanel.add(okayFeedStudentButton);

        feedStudentFrame.setLayout(new FlowLayout());
        feedStudentFrame.setSize(460, 250);

        feedStudentFrame.add(studentPicture);
        feedStudentFrame.add(buttonsPanel);
        feedStudentFrame.setIconImage(new ImageIcon("resourses/icon.png").getImage());
        feedStudentFrame.setLocationRelativeTo(null);
        feedStudentFrame.setVisible(true);

    }
}
