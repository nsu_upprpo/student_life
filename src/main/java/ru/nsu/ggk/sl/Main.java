package ru.nsu.ggk.sl;

import ru.nsu.ggk.sl.controller.ChiefWindowController;
import ru.nsu.ggk.sl.controller.PersonageModelController;

import javax.swing.*;
import java.lang.reflect.InvocationTargetException;

public class Main {
    static public void main(String[] args) {
        try {
            SwingUtilities.invokeAndWait(() -> {
                PersonageModelController personageModelController = new PersonageModelController();
                ChiefWindowController chiefWindowController = new ChiefWindowController(personageModelController);
                chiefWindowController.startWelcomeWindow();
            });
        } catch (InterruptedException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
