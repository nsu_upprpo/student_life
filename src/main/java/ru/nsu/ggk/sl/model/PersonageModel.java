package ru.nsu.ggk.sl.model;

public class PersonageModel {
    private String personageName;
    private String personageType;
    private int personageAge;

    private String ownerName;
    private String ownerGender;

    private int coinsAmount;
    private int foodAmount;
    private int drinkAmount;

    private int satietyState;
    private int achievementState;
    private int vivacityState;
    private int moodState;

    public PersonageModel() {
        satietyState = 100;
        achievementState = 100;
        vivacityState = 100;
        moodState = 100;

        foodAmount = 0;
        drinkAmount = 0;
    }

    public int getFoodAmount() {
        return foodAmount;
    }

    public int getDrinkAmount() {
        return drinkAmount;
    }

    public void setDrinkAmount(int drinkAmount) {
        this.drinkAmount = drinkAmount;
    }

    public void setFoodAmount(int foodAmount) {
        this.foodAmount = foodAmount;
    }

    public void setOwner(int coinsAmount) {
        this.coinsAmount = coinsAmount;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public String getOwnerGender() {
        return ownerGender;
    }

    public synchronized int getCoinsAmount() {
        return coinsAmount;
    }

    public synchronized int getSatietyState() {
        return satietyState;
    }

    public synchronized int getAchievementState() {
        return achievementState;
    }

    public synchronized int getVivacityState() {
        return vivacityState;
    }

    public synchronized int getMoodState() {
        return moodState;
    }


    public synchronized void setMoodState(int moodState) {
        this.moodState = moodState;
    }

    public synchronized void setSatietyState(int satietyState) {
        this.satietyState = satietyState;
    }

    public synchronized void setAchievementState(int achievementState) {
        this.achievementState = achievementState;
    }

    public synchronized void setVivacityState(int vivacityState) {
        this.vivacityState = vivacityState;
    }

    public synchronized void setCoinsAmount(int coinsAmount) {
        this.coinsAmount = coinsAmount;
    }

    public void setOwner(String ownerName, int coinsAmount, String ownerGender) {
        this.ownerName = ownerName;
        this.coinsAmount = coinsAmount;
        this.ownerGender = ownerGender;
    }

    public void setStudent(String personageName, String personageType, int personageAge) {
        this.personageName = personageName;
        this.personageType = personageType;
        this.personageAge = personageAge;
    }

    public String getStudentName() {
        return personageName;
    }

    public String getStudentType() {
        return personageType;
    }

    public int getStudentAge() {
        return personageAge;
    }
}
