package ru.nsu.ggk.sl.controller;

import ru.nsu.ggk.sl.view.FeedStudentWindowView;

import javax.swing.*;

public class FeedStudentWindowController extends AbstractWindowController{
    private FeedStudentWindowView feedStudentWindow;

    FeedStudentWindowView getFeedStudentWindow() {
        return feedStudentWindow;
    }

    @Override
    void createWindow() {
        feedStudentWindow = new FeedStudentWindowView();
    }

    @Override
    JFrame getWindow() {
        return getFeedStudentWindow();
    }

    FeedStudentWindowController() {
        createWindow();
    }
}
