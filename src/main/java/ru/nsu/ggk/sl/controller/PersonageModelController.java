package ru.nsu.ggk.sl.controller;

import ru.nsu.ggk.sl.model.PersonageModel;

import java.beans.PropertyChangeSupport;

public class PersonageModelController {
    private PersonageModel personageModel;
    private PropertyChangeSupport modelPropertyChange;

    public void accessMood() {
        if ((personageModel.getAchievementState() < 40) || (personageModel.getSatietyState() <50)) {
            int moodState = personageModel.getMoodState();
            if (moodState > 0) {
                --moodState;
            }
            personageModel.setMoodState(moodState);
        }
    }

    public void accessSatiety() {
        int satietyState = personageModel.getSatietyState();
        if (satietyState > 0) {
            --satietyState;
        }
        personageModel.setSatietyState(satietyState);
    }

    public void accessVivacity() {
        int vivacityState = personageModel.getVivacityState();
        if (vivacityState + 5 < 100) {
            vivacityState += 5;
        }
        personageModel.setVivacityState(vivacityState);
    }

    public void accessAchievement() {
            int achievementState = personageModel.getAchievementState();
            if (achievementState > 0) {
                --achievementState;
            }
            personageModel.setAchievementState(achievementState);
    }

    public PersonageModelController() {
        this.personageModel = new PersonageModel();
    }

    public PropertyChangeSupport getModelPropertyChange() {
        return modelPropertyChange;
    }
    public PersonageModel getPersonageModel() {
        return personageModel;
    }
    void setModelPropertyChange(PropertyChangeSupport modelPropertyChange) {
        this.modelPropertyChange = modelPropertyChange;
    }
}
