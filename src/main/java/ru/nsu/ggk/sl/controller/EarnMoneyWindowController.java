package ru.nsu.ggk.sl.controller;


import ru.nsu.ggk.sl.view.EarnMoneyWindowView;

import javax.swing.*;

public class EarnMoneyWindowController extends AbstractWindowController {
    private EarnMoneyWindowView earnMoneyWindow;

    @Override
    void createWindow() {
        earnMoneyWindow = new EarnMoneyWindowView();
    }

    @Override
    JFrame getWindow() {
        return getEarnMoneyWindow();
    }

    EarnMoneyWindowController() {
        createWindow();
    }

    EarnMoneyWindowView getEarnMoneyWindow() {
        return earnMoneyWindow;
    }

}
