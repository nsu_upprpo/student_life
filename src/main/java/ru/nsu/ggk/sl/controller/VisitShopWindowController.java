package ru.nsu.ggk.sl.controller;

import ru.nsu.ggk.sl.view.VisitShopWindowView;

import javax.swing.*;

public class VisitShopWindowController extends AbstractWindowController {
    private VisitShopWindowView visitShopWindowView;

    @Override
    void createWindow() {
        visitShopWindowView = new VisitShopWindowView();
    }

    @Override
    JFrame getWindow() {
        return getVisitShopWindowView();
    }

    VisitShopWindowController() {
        createWindow();
    }

    VisitShopWindowView getVisitShopWindowView() {
        return visitShopWindowView;
    }
}
