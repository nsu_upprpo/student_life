package ru.nsu.ggk.sl.controller;

import ru.nsu.ggk.sl.view.GameWindowView;

import javax.swing.*;

public class GameWindowController extends AbstractWindowController {
    private GameWindowView gameWindowView;

    @Override
    void createWindow() {
        gameWindowView = new GameWindowView();
    }

    @Override
    JFrame getWindow() {
        return getGameWindowView();
    }

    GameWindowController() {
        createWindow();
    }

    GameWindowView getGameWindowView() {
        return gameWindowView;
    }
}
