package ru.nsu.ggk.sl.controller;

import ru.nsu.ggk.sl.view.CreateStudentWindowView;

import javax.swing.*;

public class CreateStudentWindowController extends AbstractWindowController {

    private CreateStudentWindowView createStudentWindow;

    @Override
    void createWindow() {
        createStudentWindow = new CreateStudentWindowView();
    }

    @Override
    JFrame getWindow() {
        return getCreateStudentWindow();
    }

    CreateStudentWindowController() {
        createWindow();
    }

    CreateStudentWindowView getCreateStudentWindow() {
        return createStudentWindow;
    }

}
