package ru.nsu.ggk.sl.controller;

import ru.nsu.ggk.sl.view.VisitConferenceWindowView;

import javax.swing.*;

public class VisitConferenceWindowController extends AbstractWindowController {
    private VisitConferenceWindowView VisitConferenceWindow;

    @Override
    void createWindow() {
        VisitConferenceWindow = new VisitConferenceWindowView();
    }

    @Override
    JFrame getWindow() {
        return getVisitConferenceWindow();
    }

    VisitConferenceWindowController() {
        createWindow();
    }

    VisitConferenceWindowView getVisitConferenceWindow() {
        return VisitConferenceWindow;
    }

}
