package ru.nsu.ggk.sl.controller;

import ru.nsu.ggk.sl.view.VisitClubWindowView;

import javax.swing.*;

public class VisitClubWindowController extends AbstractWindowController{
    private VisitClubWindowView visitClubWindow;

    @Override
    void createWindow() {
        visitClubWindow = new VisitClubWindowView();
    }

    @Override
    JFrame getWindow() {
        return getVisitClubWindow();
    }

    VisitClubWindowController() {
        createWindow();
    }

    VisitClubWindowView getVisitClubWindow() {
        return visitClubWindow;
    }
}
