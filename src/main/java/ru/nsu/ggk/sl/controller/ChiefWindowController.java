package ru.nsu.ggk.sl.controller;

import ru.nsu.ggk.sl.updaters.*;
import ru.nsu.ggk.sl.view.*;

import java.util.ArrayList;
import java.beans.PropertyChangeSupport;
import java.util.List;
import java.util.Timer;

public class ChiefWindowController {
    private ArrayList<AbstractUpdater> updatersList;
    private AbstractWindowController feedStudentWindowController;
    private AbstractWindowController gameWindowController;
    private AbstractWindowController visitShopWindowController;
    private AbstractWindowController welcomeWindowController;
    private AbstractWindowController visitClubWindowController;
    private GamePropertyController gamePropertyController;
    private PersonageModelController personageModelController;
    private AbstractWindowController enterOwnerWindowController;
    private AbstractWindowController studyStudentWindowController;
    private AbstractWindowController earnMoneyWindowController;
    private AbstractWindowController visitConferenceWindowController;
    private AbstractWindowController createStudentWindowController;

    public ChiefWindowController(PersonageModelController personageModelController) {
        feedStudentWindowController = new FeedStudentWindowController();
        gameWindowController = new GameWindowController();
        enterOwnerWindowController = new EnterOwnerWindowController();
        visitShopWindowController = new VisitShopWindowController();
        welcomeWindowController = new WelcomeWindowController();
        visitClubWindowController = new VisitClubWindowController();
        studyStudentWindowController = new StudyStudentWindowController();
        gamePropertyController = new GamePropertyController(this);
        earnMoneyWindowController = new EarnMoneyWindowController();
        visitConferenceWindowController = new VisitConferenceWindowController();
        createStudentWindowController = new CreateStudentWindowController();
        this.personageModelController = personageModelController;
        updatersList = new ArrayList<>();
    }

    private void boundProperty(PersonageModelController personageModelController) {
        PropertyChangeSupport modelPropertyChange = new PropertyChangeSupport(this);
        modelPropertyChange.addPropertyChangeListener(gamePropertyController);
        personageModelController.setModelPropertyChange(modelPropertyChange);
        modelPropertyChange.firePropertyChange("OwnerProperty", 0, 1);
        modelPropertyChange.firePropertyChange("StudentProperty", 0, 1);

        AbstractUpdater satietyStateInfoUpdater = new SatietyStateInfoUpdater(modelPropertyChange, this);
        AbstractUpdater achievementStateInfoUpdater = new AchievementStateInfoUpdater(modelPropertyChange, this);
        AbstractUpdater vivacityStateInfoUpdater = new VivacityStateInfoUpdater(modelPropertyChange, this);
        AbstractUpdater moodStateInfoUpdater = new MoodStateInfoUpdater(modelPropertyChange, this);

        Timer moodStateInfoTimer = new Timer();
        Timer satietyStateInfoTimer = new Timer();
        Timer achievementStateInfoTimer = new Timer();
        Timer vivacityStateInfoTimer = new Timer();

        moodStateInfoTimer.scheduleAtFixedRate(new MoodStateInfoUpdater(personageModelController.getModelPropertyChange(), this), 0, 1000);
        satietyStateInfoTimer.scheduleAtFixedRate(new SatietyStateInfoUpdater(personageModelController.getModelPropertyChange(), this), 0, 500);
        vivacityStateInfoTimer.scheduleAtFixedRate(new VivacityStateInfoUpdater(personageModelController.getModelPropertyChange(), this), 0, 3000);
        achievementStateInfoTimer.scheduleAtFixedRate(new AchievementStateInfoUpdater(personageModelController.getModelPropertyChange(), this), 0, 1000);

        updatersList.add(moodStateInfoUpdater);
        updatersList.add(satietyStateInfoUpdater);
        updatersList.add(vivacityStateInfoUpdater);
        updatersList.add(achievementStateInfoUpdater);
    }

    public PersonageModelController getPersonageModelController() {
        return personageModelController;
    }

    public VisitShopWindowView getVisitShopWindow() {
        return (VisitShopWindowView) visitShopWindowController.getWindow();
    }

    GameWindowView getGameWindow() {
        return (GameWindowView) gameWindowController.getWindow();
    }

    public VisitClubWindowView getVisitClubWindow() {
        return (VisitClubWindowView) visitClubWindowController.getWindow();
    }


    public void startWelcomeWindow() {
        ((WelcomeWindowView) welcomeWindowController.getWindow()).createWelcomeWindow(this);
    }
    public void startVisitShopWindow() {
        ((VisitShopWindowView) visitShopWindowController.getWindow()).createVisitShopWindow(this);
    }

    public EnterOwnerWindowView getEnterOwnerWindow() {
        return (EnterOwnerWindowView) enterOwnerWindowController.getWindow();
    }

    public void startStudyStudentWindow() {
        ((StudyStudentWindowView) studyStudentWindowController.getWindow()).createStudyStudentWindow(this);
    }

    public void startVisitClubWindow() {
        ((VisitClubWindowView) visitClubWindowController.getWindow()).createVisitClubWindow(this);
    }

    public StudyStudentWindowView getStudyStudentWindow() {
        return (StudyStudentWindowView) studyStudentWindowController.getWindow();
    }

    public FeedStudentWindowView getFeedStudentWindow() {
        return (FeedStudentWindowView) feedStudentWindowController.getWindow();
    }

    public void startGameWindow() {
        ((GameWindowView) gameWindowController.getWindow()).createGameWindow(this);
        boundProperty(personageModelController);
    }
    public void startFeedStudentWindow() {
        ((FeedStudentWindowView) feedStudentWindowController.getWindow()).createWalkStudentWindow(this);
    }

    public EarnMoneyWindowView getEarnMoneyWindow() {
        return (EarnMoneyWindowView) earnMoneyWindowController.getWindow();
    }

    public void startEnterOwnerWindow() {
        ((EnterOwnerWindowView) enterOwnerWindowController.getWindow()).createEnterOwnerWindow(this);
    }

    public void startEarnMoneyWindow() {
        ((EarnMoneyWindowView) earnMoneyWindowController.getWindow()).createEarnMoneyWindow(this);
    }

    public VisitConferenceWindowView getVisitConferenceWindow() {
        return (VisitConferenceWindowView) visitConferenceWindowController.getWindow();
    }
    public void startVisitConferenceWindow() {
        ((VisitConferenceWindowView) visitConferenceWindowController.getWindow()).createVisitConferenceWindow(this);
    }
    public WelcomeWindowView getWelcomeWindow() {
        return (WelcomeWindowView) welcomeWindowController.getWindow();
    }

    public CreateStudentWindowView getCreateStudentWindow() {
        return (CreateStudentWindowView) createStudentWindowController.getWindow();
    }

    public void startCreateStudentWindow() {
        ((CreateStudentWindowView) createStudentWindowController.getWindow()).createSelectStudentWindow(this);
    }

}
