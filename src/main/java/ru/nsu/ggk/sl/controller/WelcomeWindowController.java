package ru.nsu.ggk.sl.controller;

import ru.nsu.ggk.sl.view.WelcomeWindowView;
import javax.swing.*;

public class WelcomeWindowController extends AbstractWindowController  {
    private WelcomeWindowView welcomeWindow;
    @Override
    void createWindow() {
        welcomeWindow = new WelcomeWindowView();
    }

    @Override
    JFrame getWindow() {
        return getWelcomeWindow();
    }

    WelcomeWindowController() {
        createWindow();
    }

    WelcomeWindowView getWelcomeWindow() {
        return welcomeWindow;
    }
}
