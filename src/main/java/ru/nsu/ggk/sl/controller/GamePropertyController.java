package ru.nsu.ggk.sl.controller;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class GamePropertyController implements PropertyChangeListener {
    private ChiefWindowController chiefWindowController;

    GamePropertyController(ChiefWindowController chiefWindowController) {
        this.chiefWindowController = chiefWindowController;
    }

    @Override
    public void propertyChange(PropertyChangeEvent changeEvent) {
        switch (changeEvent.getPropertyName()) {
            case "OwnerProperty":
                chiefWindowController.getGameWindow().getOwnerName().setText(chiefWindowController.getPersonageModelController().getPersonageModel().getOwnerName());
                chiefWindowController.getGameWindow().getOwnerAge().setText(String.valueOf(chiefWindowController.getPersonageModelController().getPersonageModel().getOwnerGender()));
                chiefWindowController.getGameWindow().getCoinsAmount().setText(String.valueOf(chiefWindowController.getPersonageModelController().getPersonageModel().getCoinsAmount()));
                chiefWindowController.getGameWindow().getFoodAmount().setText(String.valueOf(chiefWindowController.getPersonageModelController().getPersonageModel().getFoodAmount()));
                chiefWindowController.getGameWindow().getWaterAmount().setText(String.valueOf(chiefWindowController.getPersonageModelController().getPersonageModel().getDrinkAmount()));
                break;
            case "StudentProperty":
                chiefWindowController.getGameWindow().getStudentName().setText(chiefWindowController.getPersonageModelController().getPersonageModel().getStudentName());
                chiefWindowController.getGameWindow().getStudentAge().setText(String.valueOf(chiefWindowController.getPersonageModelController().getPersonageModel().getStudentAge()));
                chiefWindowController.getGameWindow().getStudentType().setText(chiefWindowController.getPersonageModelController().getPersonageModel().getStudentType());
                break;
            case "SatietyStateProperty":
                chiefWindowController.getGameWindow().getSatietyBar().setValue(chiefWindowController.getPersonageModelController().getPersonageModel().getSatietyState());
                break;
            case "AchievementStateProperty":
                chiefWindowController.getGameWindow().getAchievementBar().setValue(chiefWindowController.getPersonageModelController().getPersonageModel().getAchievementState());
                break;
            case "VivacityStateProperty":
                chiefWindowController.getGameWindow().getVivacityBar().setValue(chiefWindowController.getPersonageModelController().getPersonageModel().getVivacityState());
                break;
            case "MoodStateProperty":
                chiefWindowController.getGameWindow().getMoodBar().setValue(chiefWindowController.getPersonageModelController().getPersonageModel().getMoodState());
                break;
        }
    }

}
