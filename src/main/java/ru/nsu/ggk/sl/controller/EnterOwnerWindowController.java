package ru.nsu.ggk.sl.controller;

import ru.nsu.ggk.sl.view.EnterOwnerWindowView;

import javax.swing.*;

public class EnterOwnerWindowController extends AbstractWindowController {
    private EnterOwnerWindowView enterOwnerWindow;

    @Override
    void createWindow() {
        enterOwnerWindow = new EnterOwnerWindowView();
    }

    @Override
    JFrame getWindow() {
        return getEnterOwnerWindow();
    }

    EnterOwnerWindowController() {
        createWindow();
    }

    EnterOwnerWindowView getEnterOwnerWindow() {
        return enterOwnerWindow;
    }
}
