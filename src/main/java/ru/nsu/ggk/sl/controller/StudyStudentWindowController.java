package ru.nsu.ggk.sl.controller;

import ru.nsu.ggk.sl.view.StudyStudentWindowView;

import javax.swing.*;

public class StudyStudentWindowController extends AbstractWindowController {
    private StudyStudentWindowView studyStudentWindow;

    @Override
    void createWindow() {
        studyStudentWindow = new StudyStudentWindowView();
    }

    @Override
    JFrame getWindow() {
        return getStudyStudentWindow();
    }

    StudyStudentWindowController() {
        createWindow();
    }

    StudyStudentWindowView getStudyStudentWindow() {
        return studyStudentWindow;
    }
}
