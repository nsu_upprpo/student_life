package ru.nsu.ggk.sl.controller;

import javax.swing.*;

public abstract class AbstractWindowController {
    abstract void createWindow();
    abstract JFrame getWindow();
}
