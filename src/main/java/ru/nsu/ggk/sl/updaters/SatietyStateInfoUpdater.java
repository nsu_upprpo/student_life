package ru.nsu.ggk.sl.updaters;

import ru.nsu.ggk.sl.controller.ChiefWindowController;

import java.beans.PropertyChangeSupport;

public class SatietyStateInfoUpdater extends AbstractUpdater {
    public SatietyStateInfoUpdater(PropertyChangeSupport modelPropertyChange, ChiefWindowController chiefWindowController) {
        super(modelPropertyChange, chiefWindowController);
    }

    @Override
    void access() {
        getChiefWindowController().getPersonageModelController().accessSatiety();
    }

    @Override
    void updateInfo() {
        getModelPropertyChange().firePropertyChange("SatietyStateProperty", 0, 1);
    }

    @Override
    public void run() {
        access();
        updateInfo();
    }
}