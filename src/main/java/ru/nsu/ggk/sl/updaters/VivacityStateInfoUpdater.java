package ru.nsu.ggk.sl.updaters;

import ru.nsu.ggk.sl.controller.ChiefWindowController;

import java.beans.PropertyChangeSupport;

public class VivacityStateInfoUpdater extends AbstractUpdater {
    public VivacityStateInfoUpdater(PropertyChangeSupport modelPropertyChange, ChiefWindowController chiefWindowController) {
        super(modelPropertyChange, chiefWindowController);
    }

    @Override
    void access() {
        getChiefWindowController().getPersonageModelController().accessSatiety();
    }

    @Override
    void updateInfo() {
        getModelPropertyChange().firePropertyChange("VivacityStateProperty", 0, 1);
    }

    @Override
    public void run() {
        access();
        updateInfo();
    }
}
