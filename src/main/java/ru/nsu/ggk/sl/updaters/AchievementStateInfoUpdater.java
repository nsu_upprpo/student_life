package ru.nsu.ggk.sl.updaters;

import ru.nsu.ggk.sl.controller.ChiefWindowController;

import java.beans.PropertyChangeSupport;

public class AchievementStateInfoUpdater extends AbstractUpdater {
    public AchievementStateInfoUpdater(PropertyChangeSupport modelPropertyChange, ChiefWindowController chiefWindowController) {
        super(modelPropertyChange, chiefWindowController);
    }

    @Override
    void access() {
        getChiefWindowController().getPersonageModelController().accessAchievement();
    }

    @Override
    void updateInfo() {
        getModelPropertyChange().firePropertyChange("AchievementStateProperty", 0, 1);
    }

    @Override
    public void run() {
        access();
        updateInfo();
    }
}
