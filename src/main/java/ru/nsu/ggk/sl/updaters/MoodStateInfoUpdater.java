package ru.nsu.ggk.sl.updaters;

import ru.nsu.ggk.sl.controller.ChiefWindowController;

import java.beans.PropertyChangeSupport;

public class MoodStateInfoUpdater extends AbstractUpdater {
    public MoodStateInfoUpdater(PropertyChangeSupport modelPropertyChange, ChiefWindowController chiefWindowController) {
        super(modelPropertyChange, chiefWindowController);
    }

    @Override
    void access() {
        getChiefWindowController().getPersonageModelController().accessMood();
    }

    @Override
    void updateInfo() {
        getModelPropertyChange().firePropertyChange("MoodStateProperty", 0, 1);
    }

    @Override
    public void run() {
        access();
        updateInfo();
    }
}
